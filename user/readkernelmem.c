#include "kernel/types.h"
#include "kernel/stat.h"
#include "user/user.h"

int
main(int argc, char **argv)
{
  long addr;
  int value;

  if(argc != 2){
    fprintf(2, "usage: readkernelmem addr\n");
    exit(1);
  }
  
  addr = atol(argv[1]);
  printf("Reading kernel memory at addr %p:", addr);
  value=read_kmem(addr);
  printf("%x\n", value);

  exit(0);
}
